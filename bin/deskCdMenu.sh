#!/bin/sh
MENU_APP=${MENU_APP:-dmenu}

LS_DIR() {
	cd "$1"
	CPWD="`pwd`"

	SEL=`
	{
		echo $CPWD
		echo ~
		echo /
		echo '..'
		ls -l | awk '/^d/ { print $9 }'
		shift
	} | $MENU_APP`
	[ $? -ne 0 ] && exit 1

	if [ "$SEL" = "$CPWD" ]; then
		echo "$CPWD"
		exit
	fi

	LS_DIR "$SEL"
}

LS_DIR "${1:-`pwd`}"
