#!/bin/sh

DESKS_PER_GROUP=$1 # $[infostore.DESKS_PER_GROUP]
DESK_GROUPS=$2 # $[infostore.DGROUPS]

for j in `seq 0 $(($DESK_GROUPS - 1))`; do
  echo "+ I Test (Start) InfoStoreAdd DGROUP${j}_SAVED_DESK $(( 1 + $DESKS_PER_GROUP * $j ))"
done
