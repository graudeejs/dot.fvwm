#!/bin/sh

# Arguments: desks_per_group dgroups
DESKS_PER_GROUP=$1 # $[infostore.DESKS_PER_GROUP]
DESKS_GROUPS="$2"

for j in `seq 0 $(($DESKS_GROUPS - 1))`; do
    for i in `seq 0 $(($DESKS_PER_GROUP - 1))`; do
        echo "+ I UnSetEnv FVWM_DESK_$(( $i + $DESKS_PER_GROUP * $j ))_PWD"; \
    done
done
