#!/bin/sh

DESKS_PER_GROUP=$1 # $[infostore.DESKS_PER_GROUP]
DESK_GROUPS=$2 # $[infostore.DGROUPS]

echo 'DestroyMenu MenuToDesk'
echo 'AddToMenu MenuToDesk'
echo '+ "$[desk.name0]" Pick MoveToDesk 0 0'
for i in `seq 0 $(($DESK_GROUPS - 1))`; do
  echo "+ 'Group $(($i+1))' Popup MenuToDesk${i}"
done
for i in `seq 0 $(($DESK_GROUPS - 1))`; do
  echo "DestroyMenu MenuToDesk${i}"
  echo "AddToMenu MenuToDesk${i}"
  for j in `seq 0 $(($DESKS_PER_GROUP - 1))`; do
    echo "+ '$(($j+1))' Pick MoveToDesk 0 $(($i*3+$j+1))"
  done
done


echo 'DestroyMenu MenuGroupToDesk'
echo 'AddToMenu MenuGroupToDesk'
echo '+ "$[desk.name0]" Pick All (CurrentPage, CurrentDesk, $[w.class]) MoveToDesk 0 0'
for i in `seq 0 $(($DESK_GROUPS - 1))`; do
  echo "+ 'Group $(($i+1))' Popup MenuGroupToDesk${i}"
done
for i in `seq 0 $(($DESK_GROUPS - 1))`; do
  echo "DestroyMenu MenuGroupToDesk${i}"
  echo "AddToMenu MenuGroupToDesk${i}"
  for j in `seq 0 $(($DESKS_PER_GROUP - 1))`; do
    echo "+ '$(($j+1))' Pick All (CurrentPage, CurrentDesk, \$[w.class]) MoveToDesk 0 $(($i*3+$j+1))"
  done
done
