#!/bin/sh

DESKS_PER_GROUP=$1 # $[infostore.DESKS_PER_GROUP]
DESK_GROUPS=$2 # $[infostore.DGROUPS]

for i in `seq 1 $DESKS_PER_GROUP`; do
  echo "+ I Key F$i A    4  Function GotoDesk_Of_group $i"
  echo "+ I Key F$i WTFS 4S Function MoveToDesk_of_group $i"
done

for j in `seq 0 $(($DESK_GROUPS-1)) `; do
  echo "+ I Key F$(( 9 - $j ))	A	4	Function Switch_2_Desk_Group$j";
done
