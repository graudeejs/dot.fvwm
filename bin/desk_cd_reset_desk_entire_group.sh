#!/bin/sh

CURRENT_DESK=$(($1 - 1)) # $[desk.n]
DESK_PER_GROUP=$2 # $[infostore.DESKS_PER_GROUP]

CURRENT_GROUP=$(($CURRENT_DESK/$DESK_PER_GROUP));

START_DESK=$(($CURRENT_GROUP * $DESK_PER_GROUP))
END_DESK=$(($START_DESK + $DESK_PER_GROUP - 1))

for i in `seq $START_DESK $END_DESK`; do
    echo "UnSetEnv FVWM_DESK_${i}_PWD"
done
