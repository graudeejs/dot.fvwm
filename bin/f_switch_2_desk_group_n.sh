#!/bin/sh

DESK_GROUPS=$1 # $[infostore.DGROUPS]

for j in `seq 0 $(($DESK_GROUPS - 1))`; do
  echo "DestroyFunc Switch_2_Desk_Group$j"
  echo "AddToFunc Switch_2_Desk_Group$j"
  echo "+ I Function Save_Current_Desk_of_group"
  echo "+ I GotoDesk 0 \$[infostore.DGROUP${j}_SAVED_DESK]"
done
