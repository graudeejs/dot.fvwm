#!/bin/sh

# this script will create screenshot of each virtual page in fvwm
#   then it will merge them into one big jpg
#   then it will switch to Sx Sy page and open screenshot with Start_viewer
#   fvwm function
#
# this script requires fvwm module FvwmCommandS to be loaded
#
# To integrate in Fvwm, you can do something like this
#
# Exec exec $[FVWM_SCRIPTS]/RealBigScreenshot.sh $[desk.pagesx] $[desk.pagesy] $[page.nx] $[page.ny]

X=${1:-"1"}	# page count X
Y=${2:-"1"} # page count Y
Sx=${3:-"0"}	# from which pagex we ran this scipt
Sy=${4:-"0"}	# from which pagey we ran this scipt

MAGICK_SH="magick.sh"
TMP_SCREENS_DIR="${HOME}/tmp/fvwm/screenshots"

mkdir -p "$TMP_SCREENS_DIR"

for y in `sec 0 $(($Y - 1))`; do
    for x in `jot 0 $(($X - 1))`; do
		FvwmCommand "GotoPage $x $y"
		sleep 1
		"$MAGICK_SH" import -silent -quality 95 -window root "$TMP_SCREENS_DIR/$$_x${x}_y${y}.jpg"
		xnames="$xnames $TMP_SCREENS_DIR/$$_x${x}_y${y}.jpg"
	done

	"$MAGICK_SH" convert +append $xnames "$TMP_SCREENS_DIR/$$_y${y}.jpg"
	rm -f $xnames
	xnames=""
	ynames="$ynames $TMP_SCREENS_DIR/$$_y${y}.jpg"
done


fileName="${HOME}/pic/screenshots/$(date '+%Y.%m.%d_at_%H.%M.%S')-RealBig.jpg"
"$MAGICK_SH" convert -append $ynames $fileName
rm -f $ynames


FvwmCommand "GotoPage $Sx $Sy"

[ "$IMG_VIEWER" ] && exec "$IMG_VIEWER" "$fileName"

exit

# vim:tabstop=4:shiftwidth=4:
